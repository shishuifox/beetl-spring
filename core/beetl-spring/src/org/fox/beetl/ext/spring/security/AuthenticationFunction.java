package org.fox.beetl.ext.spring.security;

import org.beetl.core.Context;
import org.beetl.core.Function;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Spring Security &lt;authentication&gt;标签实现，注意它是个Function而不是Tag<br>
 * 该函数返回当前登陆用户的Authentication对象，对象的参数结构参见Spring Security说明
 *
 * @author Chen Rui
 */
public class AuthenticationFunction implements Function {
	/* ----- ----- ----- ----- 其他方法 ----- ----- ----- ----- */
	/**
	 * 函数调用处理方法
	 *
	 * @param paras
	 * @param context
	 * @return
	 */
	@Override
	public Object call(Object[] paras, Context context) {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		return securityContext.getAuthentication();
	}
}

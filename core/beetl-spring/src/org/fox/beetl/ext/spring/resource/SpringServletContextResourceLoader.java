package org.fox.beetl.ext.spring.resource;

import javax.servlet.ServletContext;

import org.beetl.core.GroupTemplate;
import org.beetl.core.resource.FileResourceLoader;
import org.springframework.web.context.ServletContextAware;

/**
 * Spring ServletContext Resource Loader
 *
 * @author fox
 */
public class SpringServletContextResourceLoader extends FileResourceLoader implements ServletContextAware {
	/* ----- ----- ----- ----- 属性 ----- ----- ----- ----- */
	/**
	 * Servlet上下文
	 */
	private ServletContext servletContext = null;

	/**
	 * Servlet上下文
	 *
	 * @param servletContext
	 */
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	/**
	 * 设置资源根路径，本方法不支持设置，SpringServletContextResourceLoader始终使用Web应用的webroot目录为根
	 *
	 * @param root
	 */
	@Override
	public void setRoot(String root) {
		throw new UnsupportedOperationException("unsupport setRoot!");
	}

	/* ----- ----- ----- ----- 初始化方法 ----- ----- ----- ----- */
	/**
	 * 重写原类的init方法，目的是覆盖
	 */
	@Override
	public void init(GroupTemplate groupTemplate) {
		super.setRoot(servletContext.getRealPath("/"));
		super.init(groupTemplate);
	}
}

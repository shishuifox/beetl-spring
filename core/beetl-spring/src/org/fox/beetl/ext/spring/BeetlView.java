package org.fox.beetl.ext.spring;

/**
 * Beetl Spring MVC View视图类
 *
 * @author Chen Rui
 * @deprecated 请使用org.fox.beetl.ext.spring.mvc.BeetlView {@link org.fox.beetl.ext.spring.mvc.BeetlView}
 */
@Deprecated
public class BeetlView extends org.fox.beetl.ext.spring.mvc.BeetlView {
}

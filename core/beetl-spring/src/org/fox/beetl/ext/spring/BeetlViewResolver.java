package org.fox.beetl.ext.spring;

/**
 * Beetl ViewResolver视图解析器
 *
 * @author Chen Rui
 * @deprecated 请使用org.fox.beetl.ext.spring.mvc.BeetlViewResolver {@link org.fox.beetl.ext.spring.mvc.BeetlViewResolver}
 */
@Deprecated
public class BeetlViewResolver extends org.fox.beetl.ext.spring.mvc.BeetlViewResolver {
}
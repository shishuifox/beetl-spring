package org.fox.beetl.ext.spring.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.beetl.core.Context;
import org.beetl.ext.web.WebVariable;

/**
 * 常用工具类
 *
 * @author Chen Rui
 */
public class Utils {
	/**
	 * 工具类 不可实例化
	 */
	private Utils() {
	}

	/* ----- ----- ----- ----- 工具方法 ----- ----- ----- ----- */
	/**
	 * 从Context上下文中获取request对象
	 *
	 * @param context
	 * @return
	 */
	public static HttpServletRequest getRequest(Context context) {
		return (HttpServletRequest) context.getGlobal("request");
	}

	/**
	 * 从Context上下文中获取response对象
	 *
	 * @param context
	 * @return
	 */
	public static HttpServletResponse getResponse(Context context) {
		return ((WebVariable) context.getGlobal("servlet")).getResponse();
	}
}

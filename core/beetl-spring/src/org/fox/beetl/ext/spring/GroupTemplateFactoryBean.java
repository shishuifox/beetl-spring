package org.fox.beetl.ext.spring;

/**
 * GroupTemplate工厂Bean<br>
 * 这个类不一定在Spring Web环境中使用，一般的Spring应用程序也能使用
 *
 * @author Chen Rui
 * @deprecated 请使用org.fox.beetl.ext.spring.core.GroupTemplateFactoryBean
 *             {@link org.fox.beetl.ext.spring.core.GroupTemplateFactoryBean}
 */
@Deprecated
public class GroupTemplateFactoryBean extends org.fox.beetl.ext.spring.core.GroupTemplateFactoryBean {
}

package org.fox.beetl.ext.spring;

/**
 * Beetl扩展 GroupTemplate配置器
 *
 * @author Chen Rui
 * @deprecated 请使用org.fox.beetl.ext.spring.core.ExtGroupTemplateConfig
 *             {@link org.fox.beetl.ext.spring.core.ExtGroupTemplateConfig}
 */
@Deprecated
public class ExtGroupTemplateConfig extends org.fox.beetl.ext.spring.core.ExtGroupTemplateConfig {
}

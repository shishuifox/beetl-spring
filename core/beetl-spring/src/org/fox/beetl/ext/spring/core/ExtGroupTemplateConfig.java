package org.fox.beetl.ext.spring.core;

import org.fox.beetl.ext.spring.core.AbstractGroupTemplateConfig;

/**
 * Beetl扩展 GroupTemplate配置器
 *
 * @author Chen Rui
 */
public class ExtGroupTemplateConfig extends AbstractGroupTemplateConfig {
}

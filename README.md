#beetl-spring Beetl的Spring扩展
文档地址: [http://www.cnblogs.com/shishuifox/articles/3851921.html](http://www.cnblogs.com/shishuifox/articles/3851921.html)
## 下一版本计划 1.0.5
+ 新版SpringServletContextResourceLoader（为向前兼容，可能会改一个新名字），主要是把路径操作抽象为Path实现，简化路径运算的复杂度，不再继承Beetl自带的FileResourceLoader而自行实现，以实现对BeetlViewResolver的prefix参数的支持。
+ Function接口和Tag扩展，允许函数在定义时对Object[]形式的参数进行类型，是否必须，缺省值等相关声明，并在函数调用前进行相应的参数检查

## 2014-07-25 1.0.4
### Beetl版本依赖提升到2.0.11
Beetl版本依赖提升到2.0.11，主要为部分内部细节更新，删除GroupTemplateFactoryBean和SpringBeanTagFactory代码中对Beetl旧版本做的兼容代码。
### 代码结构优化
根据Sonar的编程建议，修正一部分安全隐患和不良设计（不是OSC的sonar）
### GroupTemplateFactoryBean ExtGroupTemplateConfig
两个类移动到core包下，并增加了共同基类，原位置类仍保留，但已标注不建议继续使用
### BeetlViewResolver BeetlView
两个类移动到mvc包下，原位置类仍保留，但已标注不建议继续使用

## 2014-07-18 1.0.3
### GroupTemplateFactoryBean
GroupTemplateFactoryBean现在可以自己侦查已经部署的ExtGroupTemplateConfig实例，不需要再专门指定

## 2014-07-17 1.0.2
### Spring Security扩展支持
+ 提供sec.accessIf标签，用于指定access表达式，当前用户如果满足该表达式的权限要求，才会显示标签体内容，这个标签必须要Spring Security配置有安全表达式解析器的支持
等效于Spring Security &lt;authorize access="..."&gt;标签
+ 提供sec.urlIf标签，当前用户如果有权限访问指定url，才会显示标签体内容
等效于Spring Security &lt;authorize url="..."&gt;标签
+ sec.authentication() 函数，用于返回当前用户的登录凭证
相关配置请参见beetl-spring-web-security-exmaple示例
### SpringBeanSerializable
用于解决Spring收管Bean序列化问题，相关使用方法参见该类注释以及SpringBeanTagFactory类

## 2014-07-15
### BeetlViewResolver
增加默认contentType判定，具体请看源码
### ExtGroupTemplateConfig
新增ExtGroupTemplateConfig类及为GroupTemplateFactoryBean增加相关注入参数
该类主要为今后扩展提供支持

## 2014.07.09 update
修改GroupTemplateFactoryBean，使在Spring容器关闭时调用GroupTemplate的close方法

## 2014.07.08 update
### Beetl Spring视图及视图解析器
更新在多视图解析器下BeetlView与BeetlViewResolver初始化失败的bug
### 更新相应示例(web)，修改为多视图解析器的demo

## 2014.07.07 add
### SpringServletContextResourceLoader
类似Beetl自带的WebAppResourceLoader，使用Spring机制以避免使用ClassLoader获取WebApp路径可能存在的问题
### 更新相应示例(web)

## 2014.07.04 init
### GroupTemplateFactoryBean
GroupTemplate工厂Bean
### Beetl Spring视图及视图解析器
### SpringBeanTagFactory
将Beetl Tag交由Spring容器托管的支持
### SpELFuntion
在Beetl中执行spel表达式的函数
### 提交相应示例(simple, web)

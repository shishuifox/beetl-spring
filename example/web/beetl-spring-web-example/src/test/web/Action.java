package test.web;

import javax.servlet.http.HttpServletRequest;

import org.fox.beetl.ext.spring.mvc.BeetlViewResolver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller("testAction")
@RequestMapping("/*")
public class Action {
	@RequestMapping("index.html")
	public String index(@RequestParam(value = "type", required = false, defaultValue = "") String type,
			@RequestParam(value = "forwordType", required = false, defaultValue = "") String forwordType,
			HttpServletRequest request) {
		if ((forwordType != null) && !"".equals(forwordType)) {
			type = forwordType;
		}
		System.out.printf("type = %s, dispatcherType = %s%n", type, request.getDispatcherType().name());

		if ("cp".equals(type)) {
			// type为cp时，使用classpath下的beetl视图解析器
			return "cptemplate/index";
		} else if ("jsp".equals(type)) {
			// type为jsp时，使用jsp视图解析器
			return "jsp/index";
		} else if ("web".equals(type)) {
			// 其他情况使用beetl视图解析器
			return "template/index";
		} else if ("redirect".equals(type)) {
			// 重定向
			return BeetlViewResolver.redirect("/index.html?type=cp");
		} else if ("forward".equals(type)) {
			// 转发
			// 如果仍然用type传参，原来request的type参数也会被带过去
			return BeetlViewResolver.forward("/index.html?forwordType=cp");
		} else {
			return BeetlViewResolver.redirect("/index.html?type=cp");
		}
	}
}

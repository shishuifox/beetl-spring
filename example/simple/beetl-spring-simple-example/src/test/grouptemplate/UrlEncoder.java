package test.grouptemplate;

import java.net.URLEncoder;

/**
 * 只是个简单的Bean
 * 
 * @author Chen Rui
 */
public class UrlEncoder {
	public String encode(String text) {
		return URLEncoder.encode(text);
	}
}

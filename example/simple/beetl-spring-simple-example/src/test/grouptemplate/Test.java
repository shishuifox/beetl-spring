package test.grouptemplate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	public static void main(String[] args) {
		Log log = LogFactory.getLog(Test.class);

		ClassPathXmlApplicationContext applicationContext = null;
		try {
			applicationContext = new ClassPathXmlApplicationContext("/test/grouptemplate/application-context.xml");
			GroupTemplate groupTemplate = applicationContext.getBean(GroupTemplate.class);
			Template template = groupTemplate.getTemplate("spelTest.properties_temp");
			log.info(String.format("渲染结果:%n%s", template.render()));
		} finally {
			if (applicationContext != null) {
				applicationContext.close();
				applicationContext = null;
			}
		}
	}
}
